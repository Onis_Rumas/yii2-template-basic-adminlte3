<?php

namespace app\controllers;

use app\models\Menu;
use yii\web\HttpException;

/**
 * MenuController implements the CRUD actions for Menu model.
 */
class MenuController extends BaseController
{
    public function actionIndex()
    {
        $model = new Menu();

        return $this->render('index', ['model' => $model]);
    }

    public function actionLoadContent()
    {
        return $this->renderPartial('index-output');
    }

    public function actionCreate()
    {
        $model = new Menu();

        if ($model->load($_POST)) {
            $model->icon = "fa " . $model->icon;
            if ($model->save()) {
                \Yii::$app->getSession()->setFlash('success', "Menu baru berhasil ditambah");
            } else {
                \Yii::$app->getSession()->setFlash('error', "Menu baru gagal ditambah");
            }
        }

        return $this->redirect(['index']);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model) {
            $model->name = $_POST['name'];
            $model->controller = $_POST['controller'];
            $model->icon = $_POST['icon'];
            $model->parent_id = $_POST['parent_id'];
            $model->menu_kategori_id = $_POST['menu_kategori_id'];
            $model->save(false);
        }
    }

    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if ($model) {
            $model->is_deleted = 1;
            $model->save();
            return $this->renderPartial('index-output');
        }
    }

    public function actionSaveOrder()
    {
        $str = $_POST['str'];
        $trs = explode("||", $str);
        $no = 1;
        foreach ($trs as $tr) {
            $obj = explode("[=]", $tr);
            /** @var Menu $menu */
            $menu = $this->findModel($obj[0]);
            $menu->name = $obj[1];
            $menu->controller = $obj[2];
            $menu->parent_id = $obj[3];
            $menu->order = $no;
            $menu->icon = $obj[4];
            $menu->menu_kategori_id = $obj[5];
            $menu->save();
            $no++;
        }
    }

    /**
     * Finds the Menu model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Menu the loaded model
     * @throws HttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Menu::findOne($id)) !== null) {
            return $model;
        } else {
            throw new HttpException(404, 'The requested page does not exist.');
        }
    }
}
