<?php

namespace app\controllers;


use app\components\Photo;
use Exception;
use Gumlet\ImageResize;
use Yii;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\UploadedFile;

class UploaderController extends Controller
{
    public $enableCsrfValidation = false;

    public function actionUploadImage()
    {
        $uploaddir = Yii::getAlias("@app/web/uploads");

        $image = UploadedFile::getInstanceByName('userfile');
        if ($image) {
            if (!in_array($image->extension, ["jpg", "jpeg", "png", "pdf"])) {
                return Json::encode(["status" => "ERR", "message" => "Format tidak didukung."]);
            }

            $isImage = false;

            if ($image->extension == "jpeg" || $image->extension == "jpg" || $image->extension == "png") {
                $name = "image-" . md5(date("Y-m-d-H-i-s")) . "." . $image->extension;
                $isImage = true;
            } else {
                $name = "document-" . md5(date("Y-m-d-H-i-s")) . "." . $image->extension;
            }
            $fileName = $uploaddir . "/" . $name;

            $image->saveAs($fileName);

            if ($isImage) {
                try {
                    $image = new ImageResize($fileName);
                    $image->resizeToLongSide(1024);
                    $image->save($fileName);
                } catch (Exception $e) {

                }
            }

            return Json::encode([
                "status" => "OK",
                "file_name" => $name,
                "file_url" => Photo::get($name),
                "type" => $isImage ? "image" : "document"
            ]);
        }

        return Json::encode(["status" => "ERR"]);
    }

    public function actionUploadImageFotoProfil()
    {
        $uploaddir = Yii::getAlias("@app/web/uploads");

        $image = UploadedFile::getInstanceByName('userfile');
        if ($image) {
            if (!in_array($image->extension, ["jpg", "jpeg", "png", "pdf"])) {
                return Json::encode(["status" => "ERR", "message" => "Format tidak didukung."]);
            }

            $isImage = false;

            if ($image->extension == "jpeg" || $image->extension == "jpg" || $image->extension == "png") {
                $name = "image-" . md5(date("Y-m-d-H-i-s")) . "." . $image->extension;
                $isImage = true;
            } else {
                $name = "document-" . md5(date("Y-m-d-H-i-s")) . "." . $image->extension;
            }
            $fileName = $uploaddir . "/" . $name;

            $image->saveAs($fileName);

            if ($isImage) {
                try {
                    $image = new ImageResize($fileName);
                    $image->crop(483, 483, true);
                    $image->save($fileName);
                } catch (Exception $e) {

                }
            }

            return Json::encode([
                "status" => "OK",
                "file_name" => $name,
                "file_url" => Photo::get($name),
                "type" => $isImage ? "image" : "document"
            ]);
        }

        return Json::encode(["status" => "ERR"]);
    }

    public function actionCropImage()
    {
        $uploaddir = Yii::getAlias("@app/web/uploads");
        $name = "image-" . md5(date("Y-m-d-H-i-s")) . ".jpg";
        $fileName = $uploaddir . "/" . $name;

        $data = $_POST['imagedata'];

        $data = str_replace('data:image/jpeg;base64,', '', $data);

        $imageData = base64_decode($data);
        $source = imagecreatefromstring($imageData);
        $imageSave = imagejpeg($source, $fileName, 100);
        imagedestroy($source);

        if ($imageSave) {
            return Json::encode([
                "status" => "OK",
                "file_name" => $name,
                "file_url" => Photo::get($name),
            ]);
        } else {
            return Json::encode(["status" => "ERR"]);
        }
    }
}