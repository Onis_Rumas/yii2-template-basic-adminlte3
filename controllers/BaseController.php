<?php
/**
 * Created by PhpStorm.
 * User: feb
 * Date: 12/21/19
 * Time: 5:32 PM
 */

namespace app\controllers;


use app\models\Menu;
use app\models\RoleMenu;
use Yii;
use yii\web\Controller;

class BaseController extends Controller
{
    public function beforeAction($action)
    {
        //check login
        if (Yii::$app->user->isGuest) {
            return $this->redirect(["site/login"]);
        }

        $id = Yii::$app->controller->id;

        /** @var Menu $menu */
        $menu = Menu::find()->where(["controller" => $id])->one();
        if ($menu) {
            $role_id = Yii::$app->user->identity->role_id;
            $jmlMenu = intval(RoleMenu::find()->where(["role_id" => $role_id, "menu_id" => $menu->id])->count());
            if ($jmlMenu > 0) {
                return true;
            } else {
                return $this->redirect(["/"]);
            }
        }

        return true;
    }
}