-- Adminer 4.6.2 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `controller` varchar(50) DEFAULT NULL,
  `action` varchar(50) NOT NULL DEFAULT 'index',
  `icon` varchar(50) NOT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `parent_id` int(11) DEFAULT NULL,
  `menu_kategori_id` int(11) DEFAULT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`),
  KEY `menu_kategori_id` (`menu_kategori_id`),
  CONSTRAINT `menu_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `menu` (`id`),
  CONSTRAINT `menu_ibfk_2` FOREIGN KEY (`menu_kategori_id`) REFERENCES `menu_kategori` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `menu` (`id`, `name`, `controller`, `action`, `icon`, `order`, `parent_id`, `menu_kategori_id`, `is_deleted`) VALUES
(1,	'Dashboard',	'site',	'index',	'fas fa-tachometer-alt',	1,	NULL,	NULL,	0),
(2,	'Master Data',	'',	'index',	'fas fa-database',	2,	NULL,	1,	0),
(3,	'Menu',	'menu',	'index',	'far fa-circle',	6,	2,	1,	0),
(4,	'Setting User',	'',	'index',	'fas fa-users',	3,	NULL,	1,	0),
(5,	'Role User',	'role',	'index',	'far fa-circle',	5,	4,	1,	0),
(6,	'All User',	'user',	'index',	'fa far fa-circle',	4,	4,	1,	0);

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `menu_kategori`;
CREATE TABLE `menu_kategori` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `menu_kategori` (`id`, `nama`) VALUES
(1,	'Manajemen');

DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `role` (`id`, `name`) VALUES
(1,	'Super Administrator'),
(2,	'Administrator'),
(3,	'Employee');

DROP TABLE IF EXISTS `role_menu`;
CREATE TABLE `role_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`),
  KEY `menu_id` (`menu_id`),
  CONSTRAINT `role_menu_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`),
  CONSTRAINT `role_menu_ibfk_2` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `role_menu` (`id`, `role_id`, `menu_id`) VALUES
(1455,	2,	1),
(1456,	2,	4),
(1457,	2,	6),
(1458,	1,	1),
(1461,	1,	2),
(1462,	1,	3),
(1463,	1,	4),
(1464,	1,	6),
(1465,	1,	5);

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `photo_url` varchar(255) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `last_logout` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `user_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `user` (`id`, `role_id`, `username`, `password`, `name`, `photo_url`, `last_login`, `last_logout`, `created_at`, `updated_at`) VALUES
(1,	1,	'superadmin',	'202cb962ac59075b964b07152d234b70',	'Super Admin',	NULL,	'2021-10-11 08:56:53',	'2021-10-11 08:43:47',	'2021-10-02 14:44:16',	NULL),
(3,	2,	'admin',	'202cb962ac59075b964b07152d234b70',	'Administrator',	NULL,	'2021-10-05 09:16:05',	'2021-10-11 07:32:15',	'2021-10-02 14:46:02',	NULL);

-- 2022-04-27 07:36:53
