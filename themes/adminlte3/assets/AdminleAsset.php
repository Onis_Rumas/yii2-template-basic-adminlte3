<?php
/**
 * Created by PhpStorm.
 * User: ks
 * Date: 24/6/2561
 * Time: 1:54 น.
 */
namespace app\themes\adminlte3\assets;

use yii\web\AssetBundle;

class AdminleAsset extends AssetBundle
{
    public $sourcePath = '@app/themes/adminlte3/dist';
    public $css = [
        'plugins/overlayScrollbars/css/OverlayScrollbars.min.css',
        'css/adminlte.min.css',
    ];

    public $js = [
        'plugins/bootstrap/js/bootstrap.bundle.min.js',
        'plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js',
        'js/adminlte.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        //'yii\jui\JuiAsset',
        //'yii\bootstrap\BootstrapAsset',
        'app\themes\adminlte3\assets\FontAwesomeAsset'
    ];
}