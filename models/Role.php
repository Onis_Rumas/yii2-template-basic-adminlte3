<?php

namespace app\models;

use Yii;
use \app\models\base\Role as BaseRole;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "role".
 */


class Role extends BaseRole
{
    const SUPERADMIN = 1;
    const ADMINISTRATOR = 2;
    const EMPLOYEE = 3;
}
