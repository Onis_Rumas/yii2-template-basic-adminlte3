<?php

namespace app\models;

use Yii;
use \app\models\base\MenuKategori as BaseMenuKategori;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "menu_kategori".
 */
class MenuKategori extends BaseMenuKategori
{

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }
}
