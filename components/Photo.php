<?php

namespace app\components;

use Yii;
use yii\helpers\Url;

Yii::setAlias('@app', dirname(__DIR__));

class Photo
{    
    public static function get($file)
    {
        $file = trim($file);
        if (substr($file, 0, 4) == "http") {
            return $file;
        }
        if (file_exists(\Yii::getAlias("@app/web/uploads/" . $file)) && is_file(\Yii::getAlias("@app/web/uploads/" . $file))) {
            return Url::to(["/uploads/" . $file], true);
        } else {
            return Url::to(["/images/Empty_img_square.jpg"], true);
        }
    }
}