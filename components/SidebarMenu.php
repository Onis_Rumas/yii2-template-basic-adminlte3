<?php

namespace app\components;

use app\models\Menu;
use app\models\RoleMenu;
use app\models\MenuKategori;
use Yii;
use yii\bootstrap\Widget;

class SidebarMenu extends Widget
{
    public static function getMenu($roleId)
    {
        $hasActive = false;
        $menuWithoutKategori = SidebarMenu::menuWithoutKategori($roleId);
        $menuWithKategori = SidebarMenu::menuWithKategori($roleId);
        
        if ($menuWithoutKategori['active'] || $menuWithKategori['active']) {
            $hasActive = true;
        }

        $output = array_merge($menuWithoutKategori['menus'], $menuWithKategori['menus']);

        return ["menus" => $output, "active" => $hasActive];
    }

    private static function menuWithoutKategori($roleId)
    {
        $hasActive = false;
        $output = [];
        
        foreach (Menu::find()->where(["parent_id" => null,  'menu_kategori_id' => null])->andWhere(["is_deleted" => 0])->orderBy("`order` ASC")->all() as $menu) {
            $status = Yii::$app->controller->id == $menu->controller;
            if ($status) {
                $hasActive = true;
            }

            $obj = [
                "label" => $menu->name,
                "icon" => $menu->icon,
                "url" => SidebarMenu::getUrl($menu),
                "visible" => SidebarMenu::roleHasAccess($roleId, $menu->id),
                "active" => $status,
                "type" => "menu"
            ];

            if (count(Menu::findAll(["parent_id" => $menu->id, "is_deleted" => 0])) != 0) {
                $children = SidebarMenu::menuChild($roleId, $menu->id);
                $obj["items"] = $children['menus'];
                if ($children["active"]) {
                    $obj["active"] = true;
                    $hasActive = true;
                }
            }

            $output[] = $obj;
        }

        return ["menus" => $output, "active" => $hasActive];
    }

    private static function menuWithKategori($roleId)
    {
        $hasActive = false;
        $output = [];

        foreach (MenuKategori::find()->all() as $menuKategori) {
            $objKategori = [
                "label" => $menuKategori->nama,
                "icon" => "",
                "url" => "",
                "visible" => TRUE,
                "active" => FALSE,
                "type" => "header"
            ];

            $output[] = $objKategori;

            foreach (Menu::find()->where(["parent_id" => null,  'menu_kategori_id' => $menuKategori->id])->andWhere(["is_deleted" => 0])->orderBy("`order` ASC")->all() as $menu) {
                $status = Yii::$app->controller->id == $menu->controller;
                if ($status) {
                    $hasActive = true;
                }

                $obj = [
                    "label" => $menu->name,
                    "icon" => $menu->icon,
                    "url" => SidebarMenu::getUrl($menu),
                    "visible" => SidebarMenu::roleHasAccess($roleId, $menu->id),
                    "active" => $status,
                    "type" => "menu"
                ];

                if (count(Menu::findAll(["parent_id" => $menu->id, "is_deleted" => 0])) != 0) {
                    $children = SidebarMenu::menuChild($roleId, $menu->id);
                    $obj["items"] = $children['menus'];
                    if ($children["active"]) {
                        $obj["active"] = true;
                        $hasActive = true;
                    }
                }

                $output[] = $obj;
            }
        }

        return ["menus" => $output, "active" => $hasActive];
    }

    private static function menuChild($roleId, $parentId = NULL)
    {
        $hasActive = false;
        $output = [];
        
        foreach (Menu::find()->where(["parent_id" => $parentId])->andWhere(["is_deleted" => 0])->orderBy("`order` ASC")->all() as $menu) {
            $status = Yii::$app->controller->id == $menu->controller;

            if ($status) {
                $hasActive = true;
            }
            
            $obj = [
                "label" => $menu->name,
                "icon" => $menu->icon,
                "url" => SidebarMenu::getUrl($menu),
                "visible" => SidebarMenu::roleHasAccess($roleId, $menu->id),
                "active" => $status,
                "type" => "menu"
            ];

            $output[] = $obj;
        }

        return ["menus" => $output, "active" => $hasActive];
    }
    
    private static function roleHasAccess($roleId, $menuId)
    {
        $roleMenu = RoleMenu::find()->where(["menu_id" => $menuId, "role_id" => $roleId])->one();
        if ($roleMenu) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    private static function getUrl($menu)
    {
        if ($menu->controller == NULL) {
            return "#";
        } else {
            return [$menu->controller . "/" . $menu->action];
        }
    }
}