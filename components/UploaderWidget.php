<?php

namespace app\components;


use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;

class UploaderWidget extends Widget
{
    public $model;
    public $attribute;
    public $name;
    public $value;
    public $defaultImage;
    public $allowedExtensions = ["jpeg", "jpg", "png", "pdf"];
    public $documentImagePath;
    public $mode = 'default'; //default, nodialog
    public $aspectRatio = null; //null atau "16 / 9",

    private function getNameSelector()
    {
        if ($this->name) {
            return $this->name;
        } else {
            $model = $this->model;
            $attr = $this->attribute;
            $baseName = StringHelper::basename($model::className());
            return $baseName . "[" . $attr . "]";
        }
    }

    private function getIdSelector()
    {
        if ($this->name) {
            return $this->name;
        } else {
            $model = $this->model;
            $attr = $this->attribute;
            $baseName = StringHelper::basename($model::className());
            return strtolower($baseName) . "-" . $attr;
        }
    }

    private function getIdentifier()
    {
        if ($this->name) {
            return $this->name;
        } else {
            return $this->attribute;
        }
    }

    private function getDataValue()
    {
        if ($this->name) {
            return $this->value;
        } else {
            $model = $this->model;
            $attr = $this->attribute;
            return $model->$attr;
        }
    }

    public function init()
    {
        if ($this->defaultImage == null) {
            $this->defaultImage = Url::to(["/images/no-image.png"]);
        }

        if ($this->documentImagePath == null) {
            $this->documentImagePath = Url::to(["/images/icon-pdf.png"]);
        }

        $css = '
        
        .frame {
            box-shadow: 0px 2px 5px #ccc;
            border-radius: 4px;
            padding: 10px;
            position: relative
        }
        
        .img-frame {
            width: 100%;
        }
        
        .image-info {
            padding: 10px;
            text-align: center
        }
        
        .btn-crop {
            left: 0px;
            top: 0px;
            opacity: 0.7;
            z-index: 1;
        }
        
        .btn-view {
            right: 0px;
            top: 0px;
            opacity: 0.7;
            z-index: 1;
        }
        
        .no-z-index {
            z-index: 1 !important;
        }

        .upload-success-status-'.$this->getIdentifier().'{
            color: #fff;
            background: #4CAF50;
            padding: 10px;
            width: 40px;
            height: 40px;
            border-radius: 20px;
            display: flex;
            align-items: center;
            justify-content: center;
            position: absolute;
            bottom: 15px;
            right: 15px;
            border:solid #999 1px;
        }
        .upload-loading-status-'.$this->getIdentifier().'{
            color: #000;
            background: #ddd;
            padding: 10px;
            width: 40px;
            height: 40px;
            border-radius: 20px;
            display: flex;
            align-items: center;
            justify-content: center;
            position: absolute;
            bottom: 15px;
            right: 15px;
            border:solid #999 1px;
        }
        .upload-error-status-'.$this->getIdentifier().'{
            color: #fff;
            background: #a21639;
            padding: 10px;
            width: 40px;
            height: 40px;
            border-radius: 20px;
            display: flex;
            align-items: center;
            justify-content: center;
            position: absolute;
            bottom: 15px;
            right: 15px;
            border:solid #999 1px;
        }
        
        ';

        $this->view->registerCss($css);

        $allowed = implode("|", $this->allowedExtensions);
        $allowedInfo = implode(", ", $this->allowedExtensions);

        if ($this->mode == "default") {
            $buttonUpload = "#image-" . $this->getIdentifier();
            $croppedImage = "#image-modal-" . $this->getIdentifier();
        } else if ($this->mode == "nodialog") {
            $buttonUpload = "#btn-upload-" . $this->getIdentifier();
            $croppedImage = "#image-" . $this->getIdentifier();
        }

        $rand = rand(0, 100000);

        $js = '
        
        var cropper'.$rand.';
        
        function initCropper'.$rand.'(element){
            if(element == null){
                element = "image-' . $this->getIdentifier() . '";
            }
            var image = document.getElementById(element);
            cropper'.$rand.' = new Cropper(image, {
                autoCropArea: 1,
                '.($this->aspectRatio ? "aspectRatio: ".$this->aspectRatio."," : "").'
                ready: function () {
                    //Should set crop box data first here
                    //cropper.setCropBoxData(cropBoxData).setCanvasData(canvasData);
                }
            });
        }
        
        function destroyCropper'.$rand.'(){
            if(cropper'.$rand.'){
                cropper'.$rand.'.destroy();
            }
        }
        
        function getCroppedImage'.$rand.'(){
            //console.log(cropper'.$rand.');
            return cropper'.$rand.'.getCroppedCanvas().toDataURL("image/jpeg");
        }
        
        new AjaxUpload("' . $buttonUpload . '", {
            action: "' . Url::to(["uploader/upload-image"]) . '",
            responseType: "json",
            onSubmit : function(file , ext){
                // Allow only images. You should add security check on the server-side.
                if (ext && /^(' . $allowed . ')$/.test(ext)){
                    /* Setting data */
                    //$("#btn-upload-' . $this->getIdentifier() . '").hide();
                    $(".upload-status-'.$this->getIdentifier().'").hide();
                    $(".upload-loading-status-'.$this->getIdentifier().'").show();
                } else {
                    // extension is not allowed
                    alert("Mohon maaf, format yang didukung hanya : ' . $allowedInfo . '");
                    // cancel upload
                    return false;
                }
            },
            onComplete : function(file, response){
                if(response.status == "OK"){
                    var mode = "'.$this->mode.'";
                    
                    $("#' . $this->getIdSelector() . '").val(response.file_name);
                    
                    //enable / disable tombol crop
                    if(mode == "default"){
                        if(response.type == "image"){
                            $("#image-' . $this->getIdentifier() . '").attr("src", response.file_url);
                            $("#image-modal-' . $this->getIdentifier() . '").attr("src", response.file_url);
                            $("#crop-' . $this->getIdentifier() . '").show();
                        }else{
                            $("#image-' . $this->getIdentifier() . '").attr("src", "' . $this->documentImagePath . '");
                            $("#image-modal-' . $this->getIdentifier() . '").attr("src", "' . $this->documentImagePath . '");
                            $("#crop-' . $this->getIdentifier() . '").hide();
                        }
                        
                        $("#view-' . $this->getIdentifier() . '").attr("href", response.file_url).show();
                    }else if(mode == "nodialog"){
                        $("#image-' . $this->getIdentifier() . '").attr("src", response.file_url);
                        
                        destroyCropper'.$rand.'();
                        
                        initCropper'.$rand.'("image-' . $this->getIdentifier() . '");
                    }
                    $(".upload-status-'.$this->getIdentifier().'").hide();
                    $(".upload-success-status-'.$this->getIdentifier().'").show();
                }else{
                    alert("Upload file gagal");
                    $(".upload-status-'.$this->getIdentifier().'").hide();
                    $(".upload-error-status-'.$this->getIdentifier().'").show();
                }
                
                //$("#btn-upload-' . $this->getIdentifier() . '").show();
            }
        });
        
        $("#modal-' . $this->getIdentifier() . '").on("shown.bs.modal", function () {
            $(".btn-crop").addClass("no-z-index");
            $(".btn-view").addClass("no-z-index");
            
            initCropper'.$rand.'("image-modal-' . $this->getIdentifier() . '");
        }).on("hidden.bs.modal", function () {
            $(".btn-crop").removeClass("no-z-index");
            $(".btn-view").removeClass("no-z-index");
            destroyCropper'.$rand.'();
        });
        
        //hanya ada di mode default
        $("#btn-update-' . $this->getIdentifier() . '").click(function(){
            var hasil = getCroppedImage'.$rand.'();
            //console.log(hasil);
            destroyCropper'.$rand.'();
            
            $.ajax({
                url: "' . Url::to(["uploader/crop-image"]) . '",
                data: {
                    imagedata: hasil
                },
                type: "post",
                dataType: "json",
                success: function(response){
                    $("#image-' . $this->getIdentifier() . '").attr("src", response.file_url);
                    $("#image-modal-' . $this->getIdentifier() . '").attr("src", response.file_url);
                    $("#' . $this->getIdSelector() . '").val(response.file_name);
                    $("#view-' . $this->getIdentifier() . '").attr("href", response.file_url).show();
                }
            })
        
            $("#modal-' . $this->getIdentifier() . '").modal("hide");
            return false;
        });
        
        //hanya ada di mode nodialog
        $("#btn-crop-' . $this->getIdentifier() . '").click(function(){
            var hasil = getCroppedImage'.$rand.'()
            destroyCropper'.$rand.'();
            
            $.ajax({
                url: "' . Url::to(["uploader/crop-image"]) . '",
                data: {
                    imagedata: hasil
                },
                type: "post",
                dataType: "json",
                success: function(response){
                    $("#image-' . $this->getIdentifier() . '").attr("src", response.file_url);
                    $("#' . $this->getIdSelector() . '").val(response.file_name);
                    
                    initCropper'.$rand.'("image-' . $this->getIdentifier() . '");
                }
            });
            
            return false;
        });
        
        ';

        $this->view->registerJs($js);

        if($this->mode == "nodialog") {
            $js = '
            
            //detect modal
            var modal = $("#image-' . $this->getIdentifier() . '").closest(".modal");
            if(modal.length != 0){
                modal.on("shown.bs.modal", function () {
                    initCropper'.$rand.'("image-' . $this->getIdentifier() . '");
                }).on("hidden.bs.modal", function () {
                    console.log("Destroying Cropper");
                    destroyCropper'.$rand.'();
                });
            }
            
            $("#crop-' . $this->getIdentifier() . '").hide();
            $("#view-' . $this->getIdentifier() . '").hide();
            
            ';

            $this->view->registerJs($js);
        }
    }

    public function run()
    {
        $fileLocation = null;
        $imageUrl = $this->defaultImage;
        if ($this->getDataValue() != null) {
            $imageUrl = Photo::get($this->getDataValue());
            $fileLocation = $imageUrl;
        }

        $baseUrl = StringHelper::basename($imageUrl);
        $arr = explode(".", $baseUrl);
        $extension = $arr[count($arr) - 1];
        if ($extension == "jpeg" || $extension == "png" || $extension == "jpg") {
            $isImage = true;
        } else {
            $imageUrl = $this->documentImagePath;
            $isImage = false;
        }

        ?>

        <div class="frame">
            <div style="height: 50px; width: 100%; background-color:#fff; position: relative; margin-bottom: 10px; border-bottom:solid 1px #ddd;">
                <?= Html::a("<i class='fas fa-crop'></i>", "#", ["class" => "btn btn-success pull-left btn-crop", "data-target" => "#modal-" . $this->getIdentifier(), "data-toggle" => "modal", "id" => "crop-" . $this->getIdentifier(), "style" => $isImage ? "" : "display:none"]) ?>
                <?= Html::a("<i class='fas fa-eye'></i>", $fileLocation, ["class" => "btn btn-info pull-right btn-view", "id" => "view-" . $this->getIdentifier(), "target" => "_blank", "style" => $fileLocation ? "" : "display:none"]) ?>
            </div>
            
            <div style="display: none;" class="upload-status-<?=$this->getIdentifier()?> upload-success-status-<?=$this->getIdentifier()?>"><span><i class="fa fa-check"></i></span></div>
            <div style="display: none;" class="upload-status-<?=$this->getIdentifier()?> upload-loading-status-<?=$this->getIdentifier()?>"><span><i class="fa fa-spinner fa-spin"></i></span></div>
            <div style="display: none;" class="upload-status-<?=$this->getIdentifier()?> upload-error-status-<?=$this->getIdentifier()?>"><span><i class="fa fa-times"></i></span></div>

            <?= Html::img($imageUrl, ["class" => "img img-frame", "id" => "image-" . $this->getIdentifier()]) ?>
        </div>

        <?php
        if ($this->mode == "nodialog") {
            ?>
            <div class="image-info">
                <?= Html::a("<i class='fas fa-upload'></i> Ganti Gambar", "#", ["class" => "btn btn-success", "id" => "btn-upload-" . $this->getIdentifier()]) ?>
                <?= Html::a("<i class='fas fa-crop'></i> Crop", "#", ["class" => "btn btn-success", "id" => "btn-crop-" . $this->getIdentifier()]) ?>
            </div>
            <?php
        }
        ?>

        <?= Html::hiddenInput($this->getNameSelector(), $this->getDataValue(), ["class" => "form-control", "id" => $this->getIdSelector()]) ?>

        <div class="modal" id="modal-<?= $this->getIdentifier() ?>" tabindex="-1" role="dialog"
             aria-labelledby="modalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalLabel">Edit Gambar</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="img-container">
                            <?= Html::img($imageUrl, ["class" => "img img-frame", "id" => "image-modal-" . $this->getIdentifier()]) ?>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" id="btn-update-<?= $this->getIdentifier() ?>">
                            Simpan
                        </button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    </div>
                </div>
            </div>
        </div>

        <?php
    }

}