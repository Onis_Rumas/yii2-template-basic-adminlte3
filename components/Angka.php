<?php
namespace app\components;

/**
 * Description of Angka
 *
 * @author feb
 */
class Angka {
    public static function toNumber($str){
        $str = str_replace(array(".", ","), array("", "."), $str);
        return $str*1;
    }
    
    public static function toString($str, $decimal=2){
        return number_format($str, $decimal, ",", ".");
    }
    
    public static function toReadableHarga($str, $withSpan=TRUE){
        return "Rp ".number_format($str, 0, ",", ".");
    }
    public static function toReadableAngka($str, $withSpan=TRUE){
        return number_format($str, 0, ",", ".");
    }
}
