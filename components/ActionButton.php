<?php

namespace app\components;


use yii\helpers\Html;

class ActionButton
{
    public static function getButtons($template = '{view} {update} {delete}')
    {
        return [
            'class' => 'yii\grid\ActionColumn',
            'template' => $template,
            'buttons' => [
                'view' => function ($url, $model, $key) {
                    return Html::a("<i class='fa fa-eye'></i>", ["view", "id" => $model->id], ["class" => "btn btn-sm btn-info", "title" => "Lihat Data"]);
                },
                'update' => function ($url, $model, $key) {
                    return Html::a("<i class='fa fa-pencil-alt'></i>", ["update", "id" => $model->id], ["class" => "btn btn-sm btn-warning", "title" => "Edit Data"]);
                },
                'delete' => function ($url, $model, $key) {
                    return Html::a("<i class='fa fa-trash'></i>", ["delete", "id" => $model->id], [
                        "class" => "btn btn-sm btn-danger",
                        "title" => "Hapus Data",
                        "data-confirm" => "Apakah Anda yakin ingin menghapus data ini ?",
                        "data-method" => "POST"
                    ]);
                },
            ],
            'contentOptions' => ['nowrap' => 'nowrap', 'style' => 'text-align:center;width:140px']
        ];
    }
}