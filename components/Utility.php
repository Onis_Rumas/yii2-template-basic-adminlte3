<?php
/**
 * Created by PhpStorm.
 * User: feb
 * Date: 23/05/17
 * Time: 15.34
 */

namespace app\components;


use Yii;
use yii\helpers\Url;

class Utility
{
    public static function processError($arr)
    {
        $output = [];
        foreach ($arr as $key => $value) {
            $output[] = implode(", ", $value);
        }
        return implode(". ", $output) . ".";
    }

    public static function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
        // trim
        $text = trim($text, '-');
        // transliterate
        if (function_exists('iconv')) {
            $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        }
        // lowercase
        $text = strtolower($text);
        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);
        if (empty($text)) {
            return 'n-a';
        }
        return $text;
    }

    public static function getImageUrl($fileName)
    {
        $path = Yii::getAlias("@app/web/uploads/") . $fileName;
        if (file_exists($path) && is_file($path)) {
            return Url::to(["uploads/" . $fileName]);
        } else {
            return Url::to(["images/user-icon.png"]);
        }
    }
}