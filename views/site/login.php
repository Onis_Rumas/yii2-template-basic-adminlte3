<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\LoginForm */

$this->title = 'Sign In';

$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];

?>

<style>
    .primary-btn{
        background-color: #12B0A2;
        color: #fff;
    }
    .primary-btn:hover,
    .primary-btn:focus{
        background-color: #208880;
        color: #fff;
    }
</style>

<div class="login-box-center">
    <!-- <div style="padding: 5px 22px; display: flex; justify-content: center;"> -->
       <!--  <img class="img img-responsive" src="<?= \yii\helpers\Url::to(["/images/LOGO_MEDICAL_TOURISM_INDONESIA.png"]) ?>" style="max-width: 150px;"> -->
    <!-- </div> -->
    <div style="padding: 30px 20px 20px 20px;">
        <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false]); ?>
        <?= $form
            ->field($model, 'username', $fieldOptions1)
            ->label(false)
            ->textInput(['class' => 'text-login', 'placeholder' => $model->getAttributeLabel('username')]) ?>
        <?= $form
            ->field($model, 'password', $fieldOptions2)
            ->label(false)
            ->passwordInput(['class' => 'text-login', 'placeholder' => $model->getAttributeLabel('password')]) ?>
        <div class="row">
            <?= Html::submitButton("Masuk", ['class' => 'btn btn-info primary-btn', 'name' => 'login-button', 'style' => 'width:100%; border-radius: 10px; padding:15px;']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
