<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\Menu $model
 */

$this->title = 'Update Menu - ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Menu', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="card card-info primary-card card-outline">
    <div class="card-header">
        <h3 class="card-title"><?= Html::encode($this->title) ?></h3>
    </div>
    <?php echo $this->render('_form', [
        'model' => $model,
    ]); ?>
</div>