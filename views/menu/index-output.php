<?php

use yii\helpers\Html;
use yii\helpers\Url;

$parents = \yii\helpers\ArrayHelper::map(\app\models\Menu::find()->where(["parent_id" => null])->andWhere(["is_deleted" => 0])->orderBy("`order` ASC")->all(), "id", "name");
$kategoriMenu = \yii\helpers\ArrayHelper::map(\app\models\MenuKategori::find()->all(), "id", "nama");

?>

<?php foreach (\app\models\Menu::find()->where(["parent_id" => null])->andWhere(["is_deleted" => 0])->orderBy("`order` ASC")->all() as $menu) { ?>
    <tr data="<?= $menu->id ?>">
        <td style="padding: 10px 40px 0 10px; border-radius:10px; display: flex; flex-direction:row;">
            <div style="flex:1; padding:10px 20px; border-radius:6px; border:1px solid #bbb; margin-bottom:15px;">
                <div style="display: flex; flex-direction:row; cursor: pointer;" class="downdrop" data-id="<?= $menu->id ?>">
                    <div style="flex:1;">
                        <div style="font-weight:normal; margin-bottom:0;">&nbsp;&nbsp;<?= $menu->name; ?></div>
                    </div>
                    <div class="downdrop-icon">
                        <i class="fa fa-angle-left"></i>
                    </div>
                </div>
                <div class="downdrop-item" style="padding: 10px 0;">
                    <div style="border-top:1px solid #ccc; padding: 20px 20px 0 20px;">
                        <div class="form-group">
                            <label>Nama Menu</label>
                            <?= Html::textInput("name", $menu->name, ["class" => "form-control name", "id" => "name-" . $menu->id]) ?>
                        </div>
                        <div class="form-group">
                            <label>Controller</label>
                            <?= Html::textInput("controller", $menu->controller, ["class" => "form-control controller", "id" => "controller-" . $menu->id]) ?>
                        </div>
                        <div class="form-group">
                            <label>Ikon</label>
                            <?= Html::textInput("icon", $menu->icon, ["class" => "form-control icon", "id" => "icp-auto-" . $menu->id, "role" => "iconpicker"]) ?>
                        </div>
                        <div class="form-group">
                            <label>Induk</label>
                            <?= Html::dropDownList("parent_id", $menu->parent_id, $parents, ["class" => "form-control parent_id", "id" => "parent_id-" . $menu->id, "prompt" => "-"]) ?>
                        </div>
                        <div class="form-group">
                            <label>Kategori Menu</label>
                            <?= Html::dropDownList("menu_kategori_id", $menu->menu_kategori_id, $kategoriMenu, ["class" => "form-control menu_kategori_id", "id" => "menu_kategori_id-" . $menu->id, "prompt" => "-"]) ?>
                        </div>
                        <div style="margin-top: 30px; display: flex; flex-direction:row; align-items:center;">
                            <a href="#" class="text-danger btnHapusMenuAjax" style="flex: 1;" data-id="<?= $menu->id ?>">Hapus Menu</a>
                            <button class="btn primary-btn elevation-1 btnSimpanMenuAjax" style="padding:10px 30px;" data-id="<?= $menu->id ?>">SIMPAN</button>
                        </div>
                    </div>
                </div>
            </div>
            <div style="padding-left: 10px;">
                <div class="sorterer" style="padding:10px 15px; border-radius:8px;">
                    <i class='fas fa-arrows-alt'></i>
                </div>
            </div>
        </td>
    </tr>
    <?php foreach (\app\models\Menu::find()->where(["parent_id" => $menu->id])->andWhere(["is_deleted" => 0])->orderBy("`order` ASC")->all() as $menu2) { ?>
        <tr data="<?= $menu2->id ?>">
            <td style="padding: 10px 10px 0 40px; display: flex; flex-direction:row;">
                <div style="flex:1; padding:10px 20px; border-radius:6px; border:1px solid #bbb; margin-bottom:15px;">
                    <div style="display: flex; flex-direction:row; cursor: pointer;" class="downdrop" data-id="<?= $menu2->id ?>">
                        <div style="flex:1;">
                            <div style="font-weight:normal; margin-bottom:0;">&nbsp;&nbsp;<?= $menu2->name; ?></div>
                        </div>
                        <div class="downdrop-icon">
                            <i class="fa fa-angle-left"></i>
                        </div>
                    </div>
                    <div class="downdrop-item" style="padding: 10px 0;">
                        <div style="border-top:1px solid #ccc; padding: 20px 20px 0 20px;">
                            <div class="form-group">
                                <label>Nama Menu</label>
                                <?= Html::textInput("name", $menu2->name, ["class" => "form-control name", "id" => "name-" . $menu2->id]) ?>
                            </div>
                            <div class="form-group">
                                <label>Controller</label>
                                <?= Html::textInput("controller", $menu2->controller, ["class" => "form-control controller", "id" => "controller-" . $menu2->id]) ?>
                            </div>
                            <div class="form-group">
                                <label>Ikon</label>
                                <?= Html::textInput("icon", $menu2->icon, ["class" => "form-control icon", "id" => "icp-auto-" . $menu2->id]) ?>
                            </div>
                            <div class="form-group">
                                <label>Induk</label>
                                <?= Html::dropDownList("parent_id", $menu2->parent_id, $parents, ["class" => "form-control parent_id", "id" => "parent_id-" . $menu2->id, "prompt" => "-"]) ?>
                            </div>
                            <div class="form-group">
                                <label>Kategori Menu</label>
                                <?= Html::dropDownList("menu_kategori_id", $menu2->menu_kategori_id, $kategoriMenu, ["class" => "form-control menu_kategori_id", "id" => "menu_kategori_id-" . $menu2->id, "prompt" => "-"]) ?>
                            </div>
                            <div style="margin-top: 30px; display: flex; flex-direction:row; align-items:center;">
                                <a href="#" class="text-danger btnHapusMenuAjax" style="flex: 1;" data-id="<?= $menu2->id ?>">Hapus Menu</a>
                                <button class="btn primary-btn elevation-1 btnSimpanMenuAjax" style="padding:10px 30px;" data-id="<?= $menu2->id ?>">SIMPAN</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="padding-left: 10px;">
                    <div class="sorterer" style="padding:10px 15px; border-radius:8px;">
                        <i class='fas fa-arrows-alt'></i>
                    </div>
                </div>
            </td>
        </tr>
    <?php } ?>
<?php } ?>

<script>
    $(".downdrop").click(function() {
        var id = $(this).attr('data-id');
        $("#icp-auto-" + id).iconpicker();
        this.parentElement.querySelector(".downdrop-item").classList.toggle("downdrop-item-active");
        this.parentElement.querySelector(".downdrop-icon").classList.toggle("downdrop-icon-down");
    });

    $(".btnHapusMenuAjax").click(function(e) {
        if (confirm("Apakah anda yakin? data akan dihapus sistem secara permanen") == true) {
            var id = $(this).attr('data-id');
            $('#konten-menu').load("<?= Url::to(['delete']) ?>" + "?id=" + id);
        }
    });

    $(".btnSimpanMenuAjax").click(function(e) {
        var id = $(this).attr('data-id');
        $.ajax({
            url: "<?= Url::to(["update"])  ?>" + "?id=" + id,
            data: {
                name: $("#name-" + id).val(),
                controller: $("#controller-" + id).val(),
                icon: $("#icp-auto-" + id).val(),
                parent_id: $("#parent_id-" + id).val(),
                menu_kategori_id: $("#menu_kategori_id-" + id).val(),
            },
            type: "post",
            success: function() {
                alert("Menu berhasil disimpan");
                loadKonten();
            }
        });
        return false;
    });
</script>