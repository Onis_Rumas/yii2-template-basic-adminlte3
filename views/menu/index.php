<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$this->title = 'Menu';
$this->params['breadcrumbs'][] = $this->title;

$parents = \yii\helpers\ArrayHelper::map(\app\models\Menu::find()->where(["parent_id" => null])->andWhere(["is_deleted" => 0])->orderBy("`order` ASC")->all(), "id", "name");
$kategoriMenu = \yii\helpers\ArrayHelper::map(\app\models\MenuKategori::find()->all(), "id", "nama");

?>

<style>
    .sorterer {
        text-align: center;
        background: #E63A60;
        color: #ffffff;
        cursor: move;
    }

    table tr.sorting-row {
        background-color: #ddd;
    }
</style>

<div class="row">
    <div class="col-md-4">
        <div class="card">
            <?php $form = ActiveForm::begin(
                [
                    'id' => 'Menu',
                    'action' => ['create'],
                    'enableClientValidation' => true,
                    'errorSummaryCssClass' => 'error-summary alert alert-error'
                ]
            );
            ?>
            <div class="card-header" style="display:flex; flex-direction:row; flex-wrap:wrap; align-items:center;">
                <div style="flex:1;">
                    <div style="font-size: 24px;">Tambah Menu</div>
                </div>
            </div>
            <div class="card-body">
                <div style="padding: 0 10px 0 10px;">
                    <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label('Nama Menu') ?>
                    <?= $form->field($model, 'controller')->textInput(['maxlength' => true])->label('Controller') ?>
                    <?= $form->field($model, 'icon')->textInput(['class' => "form-control icp-auto"])->label('Ikon') ?>
                    <?= $form->field($model, 'parent_id')->dropDownList($parents, ["class" => "form-control parent_id", "prompt" => "-"])->label('Induk') ?>
                    <?= $form->field($model, 'menu_kategori_id')->dropDownList($kategoriMenu, ["class" => "form-control menu_kategori_id", "prompt" => "-"])->label('Kategori Menu') ?>
                    <?php echo $form->errorSummary($model); ?>
                </div>
                <div style="margin-top: 40px;">
                    <?= Html::submitButton('SIMPAN', ['class' => 'btn primary-btn pull-right elevation-1', 'style' => 'padding:12px 70px;']); ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
    <div class="col-md-8">
        <div class="card">
            <div class="card-header" style="display:flex; flex-direction:row; flex-wrap:wrap; align-items:center;">
                <div style="flex:1;">
                    <div style="font-size: 24px;">Daftar Menu</div>
                </div>
                <div style="flex:2;">
                    <button id="simpanBtn" class="btn primary-btn pull-right elevation-1" style="padding:10px 30px;">SIMPAN URUTAN MENU</button>
                </div>
            </div>
            <div class="card-body table-responsive">
                <table id="tableSorter" style="width: 100%;">
                    <tbody id="konten-menu">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<?php
$this->registerJs('

new RowSorter("#tableSorter", {
    handler: "td div.sorterer",
});

$("#simpanBtn").click(function(){
    var arr = [];
    $("tbody tr").each(function(){
        var obj = [];
        obj.push($(this).attr("data"));
        obj.push($(this).find(".name").val());
        obj.push($(this).find(".controller").val());
        obj.push($(this).find(".parent_id").val());
        obj.push($(this).find(".icon").val());
        obj.push($(this).find(".menu_kategori_id").val());
        arr.push(obj.join("[=]"));
    });
    $.ajax({
        url : "' . Url::to(["save-order"]) . '",
        data : {
            str : arr.join("||"),
        },
        type : "post",
        success : function(){
            alert("Urutan Menu berhasil disimpan");
            loadKonten();
        }
    });
    return false;
});

$(".icp-auto").iconpicker();

');
?>

<script>
    function loadKonten() {
        $('#konten-menu').load("<?= Url::to(['load-content']) ?>");
    }
    loadKonten();
</script>