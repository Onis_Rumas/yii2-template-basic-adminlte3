<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Role;
use app\models\SkripsiPembatalan;
use app\models\SkripsiPerpanjangan;
use app\models\Skripsi;
use app\components\Utility;

$user = Yii::$app->user->identity;
?>

<style>
  @media (min-width: 992px) {
    /*.sidebar-toggle {
      display: none;
    }*/
  }
</style>

<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
  <!-- Left navbar links -->
  <ul class="navbar-nav">
    <li class="nav-item sidebar-toggle">
      <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
    </li>
  </ul>

  <!-- Right navbar links -->
  <ul class="navbar-nav ml-auto">
    <!-- Messages Dropdown Menu -->
    <li class="nav-item dropdown">
      <a data-toggle="dropdown" href="#">
        <div class="row">
          <div style="margin:0 15px; text-align: right; color: #000;">
            <div class="brand-text" style="font-weight: bold; font-size: 16px;"><?= $user->name ?></div>
            <div class="brand-text font-weight-light" style="font-size: 12px; margin-top: -2px;"><?= $user->role->name ?></div>
          </div>
          <img src="<?= $user ? Utility::getImageUrl($user->photo_url) : Utility::getImageUrl(null) ?>" class="img-circle" alt="User Image" style="width: 33px; height: 33px; margin: 3px 20px 3px 0;">
        </div>
      </a>
      <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right" style="right: 0px; padding: 10px;">
        <a href="<?= Url::to(['/site/logout']) ?>" data-method="post" class="btn btn-info primary-btn btn-block"><i class="fas fa-sign-out-alt" style="color: #fff;"></i> Logout</a>
      </div>
    </li>
  </ul>
</nav>
<!-- /.navbar -->