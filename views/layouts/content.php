<?php

use yii\widgets\Breadcrumbs;
use app\widgets\Alert;

?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="padding: 10px 20px 10px 20px;">
  <!-- Content Header (Page header) -->
  <div class="content-header" style="margin-bottom: 10px;">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
            <?php if (isset($this->blocks['content-header'])) { ?>
                  <h1><?= $this->blocks['content-header'] ?></h1>
              <?php } else { ?>
                  <h1>
                      <?php
                      if ($this->title !== null) {
                        echo \yii\helpers\Html::encode($this->title);
                      } else {
                        echo \yii\helpers\Inflector::camel2words(
                          \yii\helpers\Inflector::id2camel($this->context->module->id)
                        );
                        echo ($this->context->module->id !== \Yii::$app->id) ? '<small>Module</small>' : '';
                      } ?>
                  </h1>
              <?php } ?>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
              <?= \app\widgets\Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                'options' => ['class' => 'breadcrumb']
              ]) ?>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid" style="padding-bottom: 80px;">

      <?= Alert::widget() ?>
      <!-- content -->
      <?= $content ?>

    </div>
    <!--/. container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->