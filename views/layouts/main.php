<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Url;
use yii\helpers\Html;

if (Yii::$app->controller->action->id === 'login') {
  /**
   * Do not use this code in your template. Remove it.
   * Instead, use the code  $this->layout = '//main-login'; in your controller.
   */
  echo $this->render(
    'main-login',
    ['content' => $content]
  );
} else {
  app\themes\adminlte3\assets\AdminleAsset::register($this);
  app\assets\AppAsset::register($this);
  $directoryAsset = Yii::$app->assetManager->getPublishedUrl('@app/themes/adminlte3/dist');
?>

  <?php $this->beginPage() ?>
  <!DOCTYPE html>
  <html lang="<?= Yii::$app->language ?>" style="height: auto;">

  <head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@400;700&display=swap" rel="stylesheet">

    <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
  </head>

  <body class="hold-transition light-mode sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
    <?php $this->beginBody() ?>
    <div class="wrapper">

      <!-- Preloader -->
      <div class="preloader flex-column justify-content-center align-items-center" style="background-color: #12B0A2;">
        <!-- <img class="animation__wobble" src="img/AdminLTELogo.png" alt="AdminLTELogo" height="60" width="60"> -->
        <div class="brand-text font-weight-light" style="font-weight: bold; font-size: 25px; color:#FFF;">Adminlte3 Template</div>
      </div>

      <?= $this->render(
        'header.php',
        ['directoryAsset' => $directoryAsset]
      ) ?>

      <?= $this->render(
        'left.php',
        ['directoryAsset' => $directoryAsset]
      ) ?>

      <?= $this->render(
        'content.php',
        ['content' => $content, 'directoryAsset' => $directoryAsset]
      ) ?>

      <!-- Main Footer -->
      <footer class="main-footer">
        <strong>Copyright &copy; <?= date('Y') ?> <a href="#">Adminlte3 Template</a>.</strong>
        All rights reserved.
        <div class="float-right d-none d-sm-inline-block">
          <b>Version</b> 1.0.0
        </div>
      </footer>
    </div>
    <!-- ./wrapper -->

    <?php $this->endBody() ?>
  </body>

  </html>
  <?php $this->endPage() ?>
<?php } ?>