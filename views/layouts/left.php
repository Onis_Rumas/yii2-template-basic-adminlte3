<?php

use app\components\Menu;
use app\components\SidebarMenu;
use yii\helpers\Url;
use app\models\User;

/** @var User $user */
$user = Yii::$app->user->identity;
?>

<style>
  .sidebar-dark-primary .nav-sidebar>.nav-item.menu-open>.nav-link,
  .sidebar-dark-primary .nav-sidebar>.nav-item:hover>.nav-link,
  .sidebar-dark-primary .nav-sidebar>.nav-item>.nav-link.active,
  .sidebar-dark-primary .nav-sidebar>.nav-item>.nav-link.active {
    background-color: #12B0A2;
    color: #fff;
  }
</style>

<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?= Url::to(["/"]) ?>" class="brand-link" style="padding-left: 20px;">
      <div class="brand-text font-weight-light" style="font-weight: bold; font-size: 20px; color:#12B0A2;">Adminlte3 Template</div>
    </a>

    <!-- Sidebar -->
    <div class="sidebar os-host os-theme-light os-host-overflow os-host-overflow-y os-host-resize-disabled os-host-scrollbar-horizontal-hidden os-host-transition" style="padding-top: 20px;"><div class="os-resize-observer-host observed"><div class="os-resize-observer" style="left: 0px; right: auto;"></div></div><div class="os-size-auto-observer observed" style="height: calc(100% + 1px); float: left;"><div class="os-resize-observer"></div></div><div class="os-content-glue" style="margin: 0px -8px; width: 249px; height: 360px;"></div><div class="os-padding"><div class="os-viewport os-viewport-native-scrollbars-invisible" style="overflow-y: scroll;"><div class="os-content" style="padding: 0px 8px; height: 100%; width: 100%;">

      <!-- Sidebar Menu -->
      <nav class="mt-2" style="padding-bottom: 30px;">
        <?php
        $items = SidebarMenu::getMenu(Yii::$app->user->identity->role_id);
        Menu::$iconClassPrefix = '';
        echo Menu::widget(
          [
            'options' => ['class' => 'nav nav-pills nav-sidebar flex-column', 'data-widget' => 'treeview', 'role' => 'menu', 'data-accordion' => 'false'],
            'items' => $items["menus"]
          ]
        ) ?>
      </nav>
      <!-- /.sidebar-menu -->
    </div></div></div><div class="os-scrollbar os-scrollbar-horizontal os-scrollbar-unusable"><div class="os-scrollbar-track"><div class="os-scrollbar-handle" style="width: 100%; transform: translate(0px, 0px);"></div></div></div><div class="os-scrollbar os-scrollbar-vertical"><div class="os-scrollbar-track"><div class="os-scrollbar-handle" style="height: 30.8032%; transform: translate(0px, 0px);"></div></div></div><div class="os-scrollbar-corner"></div></div>
    <!-- /.sidebar -->
  </aside>