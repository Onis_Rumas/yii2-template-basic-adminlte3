<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \app\components\Tabs;

/**
 * @var yii\web\View $this
 * @var common\models\Role $model
 * @var yii\widgets\ActiveForm $form
 */

?>

<?php $form = ActiveForm::begin(
    [
        'id' => 'Role',
        'layout' => 'horizontal',
        'enableClientValidation' => true,
        'errorSummaryCssClass' => 'error-summary alert alert-error'
    ]
);
?>
<div class="card-body">
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?php echo $form->errorSummary($model); ?>
</div>
<div class="card-footer">
    <?= Html::submitButton('<i class="fa fa-save"></i> Simpan', ['class' => 'btn btn-info primary-btn']); ?>
    <?= Html::a('<i class="fa fa-chevron-left"></i> Kembali', ['index'], ['class' => 'btn btn-default']) ?>
</div>

<?php ActiveForm::end(); ?>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>
    $(document).ready(function() {
        $(".form-group").addClass('row');

        function functionMaxwidth(x) {
            if (x.matches) {
                $(".control-label").attr("style", "text-align:right");
            }
        }
        var x = window.matchMedia("(min-width: 576px)")
        functionMaxwidth(x)
        x.addListener(functionMaxwidth)
    });
</script>