<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\Role $model
 */

$this->title = 'Tambah';
$this->params['breadcrumbs'][] = ['label' => 'Role User', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="card card-info primary-card card-outline">
    <div class="card-header">
        <h3 class="card-title"><?= Html::encode($this->title) ?></h3>
    </div>
    <?php echo $this->render('_form', [
        'model' => $model,
    ]); ?>
</div>