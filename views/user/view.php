<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\components\Photo;
use app\components\Tanggal;


/**
 * @var yii\web\View $this
 * @var common\models\User $model
 */

$this->title = 'User ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'User', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'View';
?>

<div class="row">
  <div class="col-md-3">

    <!-- Profile Image -->
    <div class="card card-info primary-card card-outline">
      <div class="card-body box-profile">
        <div class="text-center">
          <div class="profile-user-img img-fluid img-circle" style="width: 100px; height:100px; display: flex; justify-content:center; align-items:center;">
            <div class="img-circle" style="background: url('<?= Photo::get($model->photo_url) ?>') no-repeat center center /*fixed*/; background-size: cover; background-position: center; width: 90px; height:90px;"></div>
          </div>
        </div>
        <h3 class="profile-username text-center"><?= $model->name ?></h3>
        <p class="text-muted text-center"><?= $model->role->name ?></p>
        <!-- <ul class="list-group list-group-unbordered mb-3">
          <li class="list-group-item">
            <b>Followers</b> <a class="float-right">1,322</a>
          </li>
          <li class="list-group-item">
            <b>Following</b> <a class="float-right">543</a>
          </li>
          <li class="list-group-item">
            <b>Friends</b> <a class="float-right">13,287</a>
          </li>
        </ul>

        <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a> -->
      </div>
    </div>
  </div>
  <!-- /.col -->
  <div class="col-md-9">
    <div class="card">
      <div class="card-header p-2">
        <ul class="nav nav-pills">
          <li class="nav-item"><a class="nav-link active" href="#about" data-toggle="tab">About</a></li>
        </ul>
      </div><!-- /.card-header -->
      <div class="card-body">
        <div class="tab-content">
          <div class="active tab-pane" id="about">
            <div class="table-responsive">
              <table class="table table-bordered table-striped">
                <tbody>
                  <tr>
                    <td>Username</td>
                    <td><?= $model->username ? $model->username : ' - '; ?></td>
                  </tr>
                  <tr>
                    <td>Full Name</td>
                    <td><?= $model->name ? $model->name : ' - '; ?></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>