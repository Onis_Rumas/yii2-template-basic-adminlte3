<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use app\components\Tabs;

/**
 * @var yii\web\View $this
 * @var common\models\User $model
 * @var yii\widgets\ActiveForm $form
 */

?>

<?php $form = ActiveForm::begin(
    [
        'id' => 'User',
        'layout' => 'horizontal',
        'enableClientValidation' => false,
        'errorSummaryCssClass' => 'error-summary alert alert-error',
        'options' => ['enctype' => 'multipart/form-data'],
    ]
);
?>
<div class="card-body">

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'role_id')->dropDownList(
        \yii\helpers\ArrayHelper::map(app\models\Role::find()->all(), 'id', 'name'),
        ['prompt' => 'Select']
    ); ?>

    <?= $form->field($model, 'photo_url')->widget(app\components\UploaderFotoProfilWidget::className(), [
        "aspectRatio" => "1/1",
        "allowedExtensions" => ["jpeg", "jpg", "png", "JPEG", "JPG", "PNG"]
    ])->label('Foto (1:1)') ?>

</div>
<div class="card-footer">
    <?= Html::submitButton('<i class="fa fa-save"></i> Simpan', ['class' => 'btn btn-info primary-btn']); ?>
    <?= Html::a('<i class="fa fa-chevron-left"></i> Kembali', ['index'], ['class' => 'btn btn-default']) ?>
</div>

<?php ActiveForm::end(); ?>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>
    $(document).ready(function() {
        $(".form-group").addClass('row');

        function functionMaxwidth(x) {
            if (x.matches) {
                $(".control-label").attr("style", "text-align:right");
            }
        }
        var x = window.matchMedia("(min-width: 576px)")
        functionMaxwidth(x)
        x.addListener(functionMaxwidth)
    });
</script>