<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use app\models\Role;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\UserSearch $searchModel
 */

$this->title = 'All User';
$this->params['breadcrumbs'][] = $this->title;
?>

<p>
    <?= Html::a('<i class="fa fa-plus"></i> Tambah', ['create'], ['class' => 'btn btn-info primary-btn']) ?>
</p>

<div class="card">
    <div class="card-body">

        <?php \yii\widgets\Pjax::begin(['id' => 'pjax-main', 'enableReplaceState' => false, 'linkSelector' => '#pjax-main ul.pagination a, th a', 'clientOptions' => ['pjax:success' => 'function(){alert("yo")}']]) ?>

        <?= GridView::widget([
            'layout' => '{summary}{pager}{items}{pager}',
            'dataProvider' => $dataProvider,
            'pager' => [
                'class' => yii\widgets\LinkPager::className(),
                'firstPageLabel' => 'First',
                'lastPageLabel' => 'Last'
            ],
            'filterModel' => $searchModel,
            'tableOptions' => ['class' => 'table table-striped table-borderless table-hover'],
            'headerRowOptions' => ['class' => 'x'],
            'columns' => [
                [
                    'class' => yii\grid\DataColumn::className(),
                    'attribute' => 'Foto',
                    'value' => function ($model) {
                        return '<img class="img img-circle" src="'.app\components\Photo::get($model->photo_url).'" style="width:50px; height:50px;">';
                    },
                    'format' => 'raw',
                    'contentOptions' => ['nowrap' => 'nowrap', 'style' => 'text-align:center;width:100px']
                ],
                'name',
                [
                    'class' => yii\grid\DataColumn::className(),
                    'attribute' => 'role_id',
                    'value' => function ($model) {
                        return $model->role->name;
                    },
                    'filter' => \yii\helpers\ArrayHelper::map(Role::find()->all(), 'id', 'name'),
                    'format' => 'raw',
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view} {update} {delete}',
                    'buttons' => [
                        'view' => function ($url, $model, $key) {
                            return Html::a("<i class='fa fa-eye'></i>", ["view", "id" => $model->id], ["class" => "btn btn-info", "title" => "Detail User"]);
                        },
                        'update' => function ($url, $model, $key) {
                            return Html::a("<i class='fa fa-pencil-alt'></i>", ["update", "id" => $model->id], ["class" => "btn btn-warning", "title" => "Ubah Data"]);
                        },
                        'delete' => function ($url, $model, $key) {
                            if ($model->role_id == Role::SUPERADMIN || $model->role_id == Role::ADMINISTRATOR) {
                                return '';
                            }
                            return Html::a("<i class='fa fa-trash'></i>", ["delete", "id" => $model->id], [
                                "class" => "btn btn-danger",
                                "title" => "Hapus Data",
                                "data-confirm" => "Apakah Anda yakin ingin menghapus data ini ?",
                            ]);
                        },
                    ],
                    'contentOptions' => ['nowrap' => 'nowrap', 'style' => 'text-align:center;width:180px']
                ],
            ],
        ]); ?>
        <?php \yii\widgets\Pjax::end() ?>
    </div>
</div>